## Next.js Oneted site 만들기 프로젝트

SK플래닛 oneted site 만들기 프로젝트를
.GIF 파일로 영상을 만들어서 작성하였습니다.

**※ 목차의 개요**

**1. 웹스톰 Dev 실행
**2. 구글로그인 API를 통하여 Firebase 자동로그인****


로그인 이후 F12 개발자모드에서  

 "User is signed in: d1eZ1073ovWkBPf3bzlyBr8PET32"  

로그를 확인 스크린샷

**3. Firebase storage에 있는 채용카드그림파일을 업로드 후에
(라우터)logoUrl로 연동을 하는 영상 시연**


**4. Card 그림을 클릭하면 라우터연동으로 
 사이트 http://localhost:3000/wd/1 , 
 http://localhost:3000/wd/1wd/2 ,**
 로 연동 되는 모습을 시연한 영상



![ë_프로젝트작동1](/uploads/401a68af2847ec529d9f0e91031d24ff/ë_프로젝트작동1.gif)
------------------------------------

## 구글 시크릿 모드에서 구글로그인 영상
![ë_프로젝트작동2](/uploads/1aa1be48bae12ae4d8b354aa1490a674/ë_프로젝트작동2.gif)
------------------------------------

## 로그인 성공과 로그 확인
![ë_프로젝트작동3](/uploads/04cd21876e53a7597772b96b7c3e00bb/ë_프로젝트작동3.gif)

![8](/uploads/f751d2f74debaf5f77e11ac11a121935/8.JPG)

------------------------------------

### 라우터 wd/1, wd/2 작동 영상시연

![ë_프로젝트작동_라우터그림작동_1](/uploads/6202e25fdbb95434eee75d945038f708/ë_프로젝트작동_라우터그림작동_1.gif)

![ë_프로젝트작동_라우터그림작동_2](/uploads/24d111fd9718a686c31f5852c786c2d7/ë_프로젝트작동_라우터그림작동_2.gif)

------------------------------------

### Router 연동된 카드 (로컬)logoUrl:"image/B8.PNG"-> firebase_storage에 있는 C8.PNG로 변동

![ë_프로젝트_firebase연동모습_2](/uploads/6f2e74b16b5757c626d6527cec25c6af/ë_프로젝트_firebase연동모습_2.gif)

### firebase_storage 연동된것을 확인하는 영상
![ë_프로젝트_firebase연동모습_3](/uploads/b38e62cfab9f27078c5099741a7f0ebc/ë_프로젝트_firebase연동모습_3.gif)

### firebase_storage 클라우드 연결된것 확인 로그확인하는 영상

![ë_프로젝트_firebase연동모습_4](/uploads/0b9ee39d3fd101ba98528c659c0f96f3/ë_프로젝트_firebase연동모습_4.gif)

### 모달창 과 태그 구현 영상

![ë_프로젝트작동_태그1](/uploads/4b56539dc966872b766854cac87d4b54/ë_프로젝트작동_태그1.gif)

![ë_프로젝트작동_태그2](/uploads/2a8924c031eb3f8071199d74901c3dfe/ë_프로젝트작동_태그2.gif)
