import React, { useEffect } from 'react';
import { collection, addDoc, CollectionReference } from "firebase/firestore";
import db from '../../firebase';
import JobDetail from './JobDetail';
import ImageComponent from './ImageComponent';
import { useRouter } from 'next/router';



type Company = {
    id: number,
    name: string,
    logoUrl: string,
    description: string,
    location: string,
    location2: string
};

const BottomCardinsider = () => {
    const router = useRouter();
    const { id } = router.query;

    const companies : Company[] = [
        {
            id:1,
            name:"오너클랜",
            logoUrl:"image/B1.PNG", // 액세스 토큰 제거
            description:"고급 개발자(typescript, nodejs, MySQL)",
            location: "서울",
            location2: "한국"
        },
        {
            id:2,
            name:"널디소프트",
            logoUrl:"image/B2.PNG", // 액세스 토큰 제거
            description:"디자이너 (신입가능)",
            location: "서울",
            location2: "한국"
        }
        ,
        {
            id:3,
            name:"썬텍시스템즈코리아",
            logoUrl:"image/B3.PNG", // 액세스 토큰 제거
            description:"SW개발자",
            location: "서울",
            location2: "한국"
        }
        ,
        {
            id:4,
            name:"에프앤가이드",
            logoUrl:"image/B4.PNG", // 액세스 토큰 제거
            description:"인덱스개발팀 (인덱스 개발 및 유지보수 관리) 신입",
            location: "서울",
            location2: "한국"
        }
        ,
        {
            id:5,
            name:"메디인테크",
            logoUrl:"image/B5.PNG", // 액세스 토큰 제거
            description:"[전문연구요원 가능] C/C++, Python 최적화 엔지니어 (신입)",
            location: "서울",
            location2: "한국"
        }
        ,
        {
            id:6,
            name:"왓챠",
            logoUrl:"image/B6.PNG", // 액세스 토큰 제거
            description:"QA 매니저",
            location: "서울",
            location2: "한국"
        }
        ,
        // Add more companies if needed
    ];

    const company = companies.find((company) => company.id === Number(id));

    useEffect(() => {
        const addCompaniesToFirestore = async () => {
            // Firestore에 추가할 때는 액세스 토큰이 필요 없으므로 제거합니다.
            for (let company of companies) {
                let logoPathWithoutToken =
                    company.logoUrl.substring(0, company.logoUrl.indexOf("?"));
                let companyWithLogoPathWithoutToken =
                    {...company ,logoUrl : logoPathWithoutToken}

                try{

                    const companyCollectionRef = collection(db, 'companies') as CollectionReference<Company>;
                }catch(e){
                    console.error('Error adding document: ', e);
                }
            }
        };

        addCompaniesToFirestore();
    }, []);

    return (
        <>
            {company && <JobDetail company={company} />}

            {/*{company && <ImageComponent imageName={company.logoUrl} />}*/}
            {/*이것이 자동으로 그림 불러오는 코드*/}
        </>
    );
};

export default BottomCardinsider;
