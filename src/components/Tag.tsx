// components/Tag.tsx
import React from 'react';

const Tag = () => {
    return (
        <div>
            <br/>
            <a href="/tag_search?tag=%25ED%2587%25B4%25EC%2582%25AC%25EC%259C%25A85%2525%25EC%259D%25B4%25ED%2595%2598" className="Tag">#퇴사율5%</a>
            <a href="/tag_search?tag=51~300" className="Tag">#51~300명</a>
            <a href="/tag_search?tag=%25EC%2584%259C-%EB-A6-AC10-%EB-85-84-%EC-9D-B4-%EC-83-81" className="Tag">#설립10년이상</a>
            <a href="/tag_search?tag=%25EA%B0%A0-%EB-B3-B5" className="Tag">#커피</a>
            <a href="/tag_search?tag=%EA%B0%A0%B3%B6" className="Tag">#간식</a>
            <a href="/tag_search?tag=IT,%20contents" className="Tag">##IT, 컨텐츠</a>
        </div>
    );
};

export default Tag;
