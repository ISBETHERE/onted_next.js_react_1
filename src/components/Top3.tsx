import React, { useState } from 'react';

// Tag 컴포넌트 정의
const Tag = ({ children }) => {
    return <div className="Tag">{children}</div>;
};

const Top3 = () => {
    // 메뉴 항목 리스트
    const menuItems = [
        "개발 전체",
        "소프트웨어 엔지니어",
        "웹 개발자",
        "서버 개발자",
        "프론트엔드 개발자",
        "자바 개발자",
        "C,C++ 개발자",
        "파이썬 개발자",
        "Node.js 개발자",
        "머신러닝 엔지니어",
        "데이터 엔지니어",
        "안드로이드 개발자",
        "DevOps / 시스템 관리자",
        "시스템,네트워크 관리자",
        "iOS 개발자",
        "개발 매니저",
        "데이터 사이언티스트",
        "기술지원",
        "QA,테스트 엔지니어",
        "임베디드 개발자",
        "보안 엔지니어",
        "빅데이터 엔지니어",
        "하드웨어 엔지니어",
        "프로덕트 매니저",
        "크로스플랫폼 앱 개발자",
        "블록체인 플랫폼 엔지니어",
        "DBA",
        "웹 퍼블리셔",
        "PHP 개발자",
        "ERP전문가",
        ".NET 개발자",
        "영상,음성 엔지니어",
        "그래픽스 엔지니어",
        "CTO,Chief Technology Officer",
        "VR 엔지니어",
        "BI 엔지니어",
        "루비온레일즈 개발자",
        "CIO,Chief Information Officer"
    ];
    const maxWordsPerLine = 7;


    const processedMenuItems = menuItems.map(item => {
        let words = item.split(' ');
        let newItem = '';

        for (let i=0; i<words.length; i++) {
            newItem += words[i] + ' ';
            if ((i+1) % maxWordsPerLine === 0) newItem += '';
        }

        return newItem;

    });

    // 모달 창이 보이는지 여부를 결정하는 상태 변수입니다.
    const [isModalVisible, setIsModalVisible] = useState(false);

    // 버튼 클릭 시 모달 창의 보이는 상태를 토글하는 함수입니다.
    const toggleModalVisibility = () => {
        setIsModalVisible(!isModalVisible);
    };

    return (
        <>
            {!isModalVisible &&
            <div className='Top2'>
                <div className="buttonContainer" style={{ display: 'flex', alignItems: 'center' }}>
                    <button
                        type="button"
                        aria-label="popup navigation button"
                        onClick={toggleModalVisibility}
                    >
                        <span>개발전체</span>
                    </button>
                    <div className="Top2svg">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 12 12">
                            <path fill="#767676" fill-rule="nonzero"
                                  d="M2.28 3.22a.75.75 0 0 0-1.06 1.06l4.25 4.25a.75.75 0 0 0 1.06 0l4.25-4.25a.75.75 0 0 0-1.06-1.06L6 6.94 2.28 3.22z"></path> </svg>
                            </div>
                        </div>
                    </div>
                }

            {isModalVisible &&
            <section role="navigation" className='Top3 isKR'>
                {menuItems.map((item, index) => (
                    <>
                        <Tag key={index}>
                            {item}
                        </Tag>
                        {(index + 1) % 6 === 0 && <br/>}
                    </>
                ))}
                <button onClick={toggleModalVisibility}>Close</button>
            </section>
                }
            </>
      );
}

export default Top3;
