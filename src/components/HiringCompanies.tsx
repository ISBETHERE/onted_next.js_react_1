// HiringCompanies.jsx

import React from 'react';


const HiringCompanies = () => {
    const companies = [
        { id:1, name:"Company A", logoUrl:"/C1.PNG", description:"This is company A."},
        { id:2, name:"Company B", logoUrl:"/C2.PNG", description:"This is company B."},
        { id:3, name:"Company C", logoUrl:"/C3.PNG", description:"This is company C."},
        { id:4, name:"Company D", logoUrl:"/C4.PNG", description:"This is company D."},
        { id:5, name:"Company E", logoUrl:"/C5.PNG", description:"This is company E."},
    ];

    return (
        <div>
            <h3>적극 채용 중인 회사</h3>
            <div className="company-list">
                {companies.map(company => (
                    <div key={company.id} className="company-card">
                        <img src={company.logoUrl} alt={`${company.name} logo`} />
                        {/*<h4>{company.name}</h4>*/}
                        {/*<p>{company.description}</p>*/}
                    </div>
                ))}
            </div>
        </div>
    );
};

export default HiringCompanies;