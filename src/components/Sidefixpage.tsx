import React from 'react';

const Sidefixpage = () => {
    return (
        <div className="Sidefixpage1">
            <div className="Siderfixpage2">
                <img src="/insider1.PNG" alt="Insider Image" style={{ width: '540px', height: '330px' }} />
                <div style={{height: '10px'}} />
                <img src="/insider2.PNG" alt="Insider Image" />
            </div>
        </div>
    );
};

export default Sidefixpage;
