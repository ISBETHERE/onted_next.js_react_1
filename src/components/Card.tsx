// In CardProps interface:
interface CardProps {
    id:number;
    name:string;
    location:string;
    industry:string;
    image?:string; // Add this line
    logo?:string; // Add this line
}

// In Card component:
const Card : React.FC<CardProps> = ({ id,name,location,industry,image,logo }) => { // Include the new props in the destructuring here.
    return (
        <a href={`/company/${id}`} className="Featured_sliderItem__FY8yb" aria-label="featured company card link">
            <header>
                {/* 배경 이미지는 public 폴더의 c1.PNG 파일을 사용합니다. */}
                <img src={image || "/C1.PNG"} alt="background" style={{width:'10%'}}/>
            </header>
            <footer>
                {/* 로고 이미지도 public 폴더의 c1.PNG 파일을 사용합니다. 실제로는 각기 다른 이미지 파일을 사용해야 할 것입니다. */}
                {/*<img src={logo || "/C1.PNG"} alt="logo" style={{width: '100px', height:'50px'}}/>*/}
                {/*<h4>{name}</h4>  /!* 기업 이름 *!/*/}
                {/* 포지션 수 정보가 없으므로 임의로 설정합니다. 실제로는 props에서 받아와야 할 것입니다. */}
                <h5>{industry}</h5>  /* 업종 정보 */
            </footer>
        </a>
    );
};

export default Card;
