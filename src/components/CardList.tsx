// src/components/CardList.tsx

import React from 'react';
import Card from './Card';

interface Company {
    id: number;
    name: string;
    location: string;
    industry: string;
    image: string; // Add this line
    logo: string; // Add this line
}

interface CardListProps {
    companies: Company[];
}

const CardList : React.FC<CardListProps> = ({companies}) => {
    return (
        <div className="card-list">
            {companies.map(company =>
                <Card key={company.id} {...company} />
            )}
        </div>
    );
};

export default CardList;