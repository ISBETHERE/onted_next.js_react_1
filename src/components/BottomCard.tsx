import React, { useEffect, useState } from 'react';
import { getStorage, ref, getDownloadURL } from "firebase/storage";
import Link from 'next/link';

const BottomCard = () => {
    const [companies, setCompanies] = useState([]);

    useEffect(() => {
        const fetchLogos = async () => {
            const storage = getStorage();
            const companiesData = [
                {
                    id:1,
                    name:"오너클랜",
                    logoUrl:"image/B1.PNG", // 액세스 토큰 제거
                    description:"고급 개발자(typescript, nodejs, MySQL)",
                    location: "서울",
                    location2: "한국"
                }
                ,
                {
                    id:2,
                    name:"널디소프트",
                    logoUrl:"image/B2.PNG", // 액세스 토큰 제거
                    description:"디자이너 (신입가능)",
                    location: "서울",
                    location2: "한국"
                }
                ,
                {
                    id:3,
                    name:"썬텍시스템즈코리아",
                    logoUrl:"image/B3.PNG", // 액세스 토큰 제거
                    description:"SW개발자",
                    location: "서울",
                    location2: "한국"
                }
                ,
                {
                    id:4,
                    name:"에프앤가이드 FnGuide",
                    logoUrl:"image/B4.PNG", // 액세스 토큰 제거
                    description:"인덱스개발팀 (인덱스 개발 및 유지보수 관리) 신입",
                    location: "서울",
                    location2: "한국"
                }
                ,
                {
                    id:5,
                    name:"메디인테크",
                    logoUrl:"image/B5.PNG", // 액세스 토큰 제거
                    description:"[전문연구요원 가능] C/C++, Python 최적화 엔지니어 (신입)",
                    location: "서울",
                    location2: "한국"
                }
                ,
                {
                    id:6,
                    name:"왓챠",
                    logoUrl:"image/B6.PNG", // 액세스 토큰 제거
                    description:"QA 매니저",
                    location: "서울",
                    location2: "한국"
                }
                ,
            ];

            for (let company of companiesData) {
                const gsReference = ref(storage, company.logoUrl);
                company.logoUrl = await getDownloadURL(gsReference);
            }

            setCompanies(companiesData);
        };

        fetchLogos();
    }, []);

    return (
        <div className="BottomCard">
            {/*<h3>적극 채용 중인 회사</h3>*/}
            <ul className="company-list" style={{display:'flex', flexWrap:'wrap', padding:'0'}}>
                {companies.map(company => (
                    <>
                    <li key={company.id} className="company-card" style={{width:'23%', listStyleType:'none', margin:'1%'}}>
                        <Link href={`/wd/${company.id}`}>
                        <img src={company.logoUrl} alt={`${company.name} logo`} />
                        <div style={{height: '10px'}} />
                        <div className="BottomCardHead">{company.description}</div>
                        <h4>{company.name}</h4>

                        <span className="addressDot">{company.location}</span>
                        <span className="addressDot">.</span>
                        <span className="addressDot">{company.location2}</span>
                        <span>{company.reward}</span>
                        </Link>
                    </li>
                    </>
                ))}
            </ul>
        </div>
    );
};

export default BottomCard;
