// TopSection.js
import React from 'react';
import ToptreeMenu from '../pages/ToptreeMenu';
import Toplogo from '../pages/Toplogo';
import Topmenu from '../pages/Topmenu';

export default function TopSection() {
    return (
        <div role="presentation" className="Topbackground Topbackground2">

            <div className="TopMenu">
                <Toplogo/>
                <ToptreeMenu/>
            </div>

            <div className="Topnav">
                <Topmenu/>
            </div>
            {/*//설리번코드*/}
        </div>
    );
}
