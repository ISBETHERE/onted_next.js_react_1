import {useEffect, useState} from "react";

const Top2 = () => {
    const [modalVisible, setModalVisible] = useState(false);

    const handleOpenModal = (e) => {
        e.stopPropagation(); // 추가: 부모 요소로의 이벤트 전파를 막음
        setModalVisible(true);
    };

    const handleCloseModal = (e) => {
        if (!e.target.closest('.Top2side2')) { // 추가: 클릭한 요소가 .Top2side2 내부에 있는 경우는 제외
            setModalVisible(false);
        }
    };

    useEffect(() => {
        if (modalVisible) {
            window.addEventListener('click', handleCloseModal);
        }

        return () => {
            window.removeEventListener('click', handleCloseModal);
        };

    }, [modalVisible]);



    return (
        <div className="Top2">
            <div className="buttonContainer" style={{ display: 'flex', alignItems: 'center' }}>
                <button
                    className=""
                    type="button"
                    aria-label="popup navigation button"
                    onClick={handleOpenModal}
                >
                    <span>개발</span>
                </button>
                <div className="Top2svg">
                    <svg xmlns="https://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 12 12"
                         className="">
                        <path fill="#767676" fill-rule="nonzero"
                          d="M2.28 3.22a.75.75 0 0 0-1.06 1.06l4.25 4.25a.75.75 0 0 0 1.06 0l4.25-4.25a.75.75 0 0 0-1.06-1.06L6 6.94 2.28 3.22z"></path>
                </svg>
            </div>
                {modalVisible &&
                <section role="navigation" class="Top2side2 isKR">
                    <a href="/wdlist" class="Top2side1" data-attribute-id="jobCategory__click" data-job-category-id="all" data-job-category="all">전체</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">개발</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">경영 비지니스</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">마케팅광고</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">디자인</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">영업</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">고객서비스 리테일</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">미디어</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">엔지니어링 설계</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">게임 제작</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">HR</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">엔지니어링 설계</a>
                    <a href="/wdlist" className="Top2side1" data-attribute-id="jobCategory__click"
                       data-job-category-id="all" data-job-category="all">엔지니어링 설계</a>



                    <button onClick={handleCloseModal}>Close</button>
                </section>}

            </div>

        </div>
    );
};

export default Top2;
