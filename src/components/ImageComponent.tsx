import { getStorage, ref, getDownloadURL } from "firebase/storage";
import React, { useEffect, useState } from 'react';

const ImageComponent = ({ imageName }: { imageName: string }) => {
    const [imageUrl, setImageUrl] = useState<string | null>(null); // <--- 수정된 부분

    useEffect(() => {
        const fetchImage = async () => {
            if (!imageName) return;
            const storage = getStorage(); // Firebase storage 인스턴스 생성
            const imageRef = ref(storage, imageName); // 이미지 참조 생성

            try {
                const url = await getDownloadURL(imageRef); // 이미지 URL 가져오기
                setImageUrl(url);
            } catch (error) {
                console.error('Error fetching image: ', error);
            }
        };

        fetchImage();
    }, [imageName]);

    return (
        <>
            {imageUrl ? <img className="image-margin" src={imageUrl} alt="From firebase"  style={{width: '725px', height: '500px'}} /> : <p>Loading...</p>}
        </>
    );
};

export default ImageComponent;
