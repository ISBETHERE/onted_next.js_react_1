import React from 'react';
import { useRouter } from 'next/router';
import TopSection from "./TopSection";
import ImageComponent from './ImageComponent';
import Tag from './Tag';
import OnerClan from "../pages/wd/OnerClan";
import Sidefixpage from "./Sidefixpage";
import Nulldy from "../pages/wd/Nulldy";
import Suntech_Systems_Korea from "../pages/wd/Suntech_Systems_Korea";
import Toplogo from "../pages/Toplogo";
import ToptreeMenu from "../pages/ToptreeMenu";
import Top2 from "./Top2";
import FnGuide from "../pages/wd/FnGuide";
import MediIntech from "../pages/wd/MediIntech";




// 가능한 모든 JobDetailX 컴포넌트를 포함하는 객체
const components = {
    1: OnerClan,
    2: Nulldy,
    3: Suntech_Systems_Korea,
    4: FnGuide,
    5: MediIntech,
    6: MediIntech,



};

const JobDetail = ({company}) => {
    const router = useRouter();
    const { id } = router.query; // URL에서 id 값을 추출

    // id에 해당하는 컴포넌트 선택
    const ComponentToRender = components[id];
    if (!id || !ComponentToRender) return null; // id값이 없거나 해당하는 컴포넌트가 없다면 아무것도 렌더링하지 않음


    return (
        <div style={{padding: '40px 80px 50px 80px'}}>
            {/* 패딩: 위 80px, 오른쪽 80px, 아래 50px, 왼쪽 20px */}

            <div className="Topfilter1 Topfilter2" style={{paddingTop:'0px', paddingLeft: '80px', paddingRight: '80px'}}>
                <div>
                    <div className="MenuItem">
                        <Toplogo/>
                        <ToptreeMenu/>
                        <a href="#" className="MenuLink">채용</a>
                        <a href="#" className="MenuLink">이벤트</a>
                        <a href="#" className="MenuLink">이력서</a>
                        <a href="#" className="MenuLink">소셜</a>
                        <a href="#" className="MenuLink">프리랜서AI</a>
                        <a href="#" className="MenuLink">합격예측</a>
                    </div>
                </div>
            </div>

            <div style={{height: '60px'}}/>
            <div style={{display: 'flex'}}>
                <Sidefixpage style={{width:'340px', height:'100px'}}/>
            </div>
                <div>

                    <ImageComponent imageName={company.logoUrl}/>
                    <h3>{company.description}</h3>
                    <p>{company.name}</p>
                    <div className="location">
                        <span>{company.location}</span>
                        <span className="addressDot">.</span>
                        <span>{company.location2}</span>
                    </div>

                    <div>
                        <Tag/>
                        {company && <ComponentToRender company={company} />}
                        {/*<OnerClan/>*/}

                    </div>
                </div>

        </div>
    );
};

export default JobDetail;
