//코딩베낌방지 설리번 코드
import React, {useEffect} from 'react';

import Topmenu from '../pages/Topmenu';


import Toplogo from "./Toplogo";
import ToptreeMenu from "./ToptreeMenu";
import HiringCompanies from "../components/HiringCompanies";
import BottomCard from "../components/BottomCard";
import {useRouter} from "next/router";
import Top2 from "../components/Top2";
import Top3 from "../components/Top3";



const BottomCardinsider = () => {
    const router = useRouter()
    const { id } = router.query

    // ...
}

// const companies = [
//     {"id": 10238, "name": "Company A", "location": "Seoul", "industry": "IT"},
//     {"id": 178911, "name": "Company B", "location": "Busan", "industry": "Manufacturing"}
// ];

//설리번코드
export default function HomePage() {
    useEffect(() => {
        const links = document.querySelectorAll('a');

        links.forEach(link => {
            link.addEventListener('mouseover', function () {
                this.style.color = 'blue';
            });

            link.addEventListener('click', function () {
                this.style.transform = 'scale(2.1)';
            });
        });
    }, []);

//설리번코드

    return (
        // className="px-4 bg-gray-100"
        <div>

            <div className="Topfilter1 Topfilter2" style={{paddingTop:'0px', paddingLeft: '80px', paddingRight: '80px'}}>
                <div>
                    <div className="MenuItem">
                        <Toplogo/>
                        <ToptreeMenu/>
                        <a href="#" className="MenuLink">채용</a>
                        <a href="#" className="MenuLink">이벤트</a>
                        <a href="#" className="MenuLink">이력서</a>
                        <a href="#" className="MenuLink">소셜</a>
                        <a href="#" className="MenuLink">프리랜서AI</a>
                        <a href="#" className="MenuLink">합격예측</a>
                    </div>
                    <div className="Top2" style={{padding: '20px 10px'}}><Top2/> <Top3/></div>
                </div>
            </div>

            <div>
                <div className="HiringCompanies" style={{paddingTop:'200px', paddingLeft: '80px', paddingRight: '80px'}}>
                <HiringCompanies/> {/* 새로 추가한 컴포넌트를 사용합니다. */}

                </div>

                <div style={{padding: '20px 80px'}}> {/* padding 스타일을 직접 추가 */}
                    {/*<CardList companies={companies}/>*/}
                    <BottomCard/>
                </div>

            </div>
            {/* 카드 리스트 추가 */}
        </div>
    );
}
//코딩베낌방지 설리번 코드