import { initializeApp, getApps } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth, GoogleAuthProvider, signInWithPopup, onAuthStateChanged } from 'firebase/auth';
import { getStorage, ref, getDownloadURL } from 'firebase/storage'; // 추가: firebase storage
import firebaseConfig from '../../firebase'; // Your Firebase configuration object
import {useEffect} from "react";
import '../styles/globals.css';


let app;

// Check if a Firebase instance doesn't exist already
if (!getApps().length) {
    app = initializeApp(firebaseConfig);
} else {
    app = getApps()[0];
}

const db = getFirestore(app);
const storage = getStorage(app); // 추가: firebase storage

function MyApp({ Component, pageProps }) {
    useEffect(() => {
        const auth = getAuth();
        const provider = new GoogleAuthProvider();

        signInWithPopup(auth, provider)
            .then((result) => {
                // User is signed in.
                console.log("User is signed in: " + result.user.uid);

                if (result.user) {
                    const imageRef = ref(storage, `image/B1.PNG`);

                    // Get the download URL for the image file
                    getDownloadURL(imageRef).then((url) => {
                        console.log('Image download URL:', url);
                        // Now you can use this URL to download the image or display it in an <img> element etc.
                    }).catch((error) => {
                        console.error('Error:', error.message);
                    });
                }
            })
            .catch((error) => {
                // Handle Errors here.
                console.error('Error:', error.message);
            });

        onAuthStateChanged(auth, (user) => {
            if (user) {
                // User is still signed in.
                console.log("User is still signed in: " + user.uid);

                const imageRef = ref(storage, `image/B1.PNG`);

                // Get the download URL for the image file
                getDownloadURL(imageRef).then((url) => {
                    console.log('Image download URL:', url);
                    // Now you can use this URL to download the image or display it in an <img> element etc.
                }).catch((error) => {
                    console.error('Error:', error.message);
                });

            } else {
                // User is signed out.
                console.log('User is signed out');
            }
        });
    }, []);

    return <Component {...pageProps} />
}

export default MyApp;
