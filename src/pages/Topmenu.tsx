import Link from 'next/link';

const Topmenu = () => {
    return (
        <div>
            <div><Link href="/wdlist" passHref>채용</Link></div>
            <div><Link href="/wdlist" passHref>이벤트</Link></div>
            <div><Link href="/wdlist" passHref>이력서</Link></div>
            <div><Link href="/wdlist" passHref>소셜</Link></div>
            <div><Link href="/wdlist" passHref>프리랜서AI</Link></div>
            <div><Link href="/wdlist" passHref>합격예측</Link></div>
        </div>
    );
};

export default Topmenu;
