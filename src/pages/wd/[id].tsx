//코딩베낌방지 설리번 코드
import { useRouter } from 'next/router';
import BottomCardinsider from '../../components/BottomCardinsider';

export default function Company() {
    const router = useRouter();
    const { id } = router.query;

    return <BottomCardinsider id={id} />;
}
