// pages/ToptreeMenu.tsx

import React, { useState } from 'react';

const NavBar = () => {
    const [isOpen, setIsOpen] = useState(false);

    // 메뉴 항목 리스트
    const menuItems = [
        "직군 전체",
        "개발",
        "경영·비즈니스",
        "마케팅·광고",
        "디자인",
        "영업",
        "고객서비스·리테일",
        "미디어",
        "엔지니어링·설계",
        "게임 제작",
        "HR",
        "금융",
        "제조·생산",
        "물류·무역",
        "교육"
    ];

    return (
        <div>
            <button
                type="button"
                aria-label="직군/직무 목록 열기"
                aria-controls="navbar_overlay_explore"
                aria-expanded={isOpen}
                class="Menu_MenuExpandableButton__vPEIA"
                onClick={() => setIsOpen(!isOpen)}
            >
              <span>
                  <span class="SvgIcon_SvgIcon__root__8vwon">
               <img src="../../Toptreemenu.PNG" alt='Toptreemenu'/>
                  </span>
              </span>
            </button>

            {isOpen && (
                <div role='navigation' className='Top2side2 isKR'>
                    {/* map 함수를 이용하여 메뉴 항목들을 렌더링합니다 */}
                    {menuItems.map((item, index) => (
                        <a key={index} className='Top2side1' href={`/${item}`}>{item}</a>
                    ))}
                </div>
            )}
        </div>
    );
};

export default NavBar;

//코딩베낌방지 설리번 코드